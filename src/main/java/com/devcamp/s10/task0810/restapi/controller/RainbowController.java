package com.devcamp.s10.task0810.restapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")
public class RainbowController {
    @GetMapping("/rainbow")
    public ArrayList<String> getRainBowColor() {
        ArrayList<String> colors = new ArrayList<>();
        colors.add("red");
        colors.add("blue");
        colors.add("purple");
        colors.add("yellow");
        colors.add("orange");
        colors.add("green");
        colors.add("pink");

        return colors;

    }

}
